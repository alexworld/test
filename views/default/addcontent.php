<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="content-language" content="ru">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>
    <body>
        <div style="width:100%;padding-top:20px;padding-bottom:50px;">
            <div style="float:left;padding-left:30px;"><h1>Test.loc Add Panel</h1></div>
            <div style="float:right;padding-right:30px;"><a href="<?php echo SITE_NAME;?>/log-in">Назад</a></div>
            <div style="clear:both;"></div>
        </div>
        <div style="width:100%;padding-top:20px;padding-bottom:50px;"> 
        <form action="" method="post">           
            <div class="container">
                <div class="row"> 
                        <div class="col"> 
                            <input type="hidden" name='add' value="1">
                            <font style="display:block">Имя пользователя</font>
                            <input type="text" name='user'>                            
                        </div> 
                </div>
                <div class="row"> 
                        <div class="col">                             
                            <font style="display:block">Email</font>
                            <input type="text" name='email'>                            
                        </div> 
                </div>
                <div class="row"> 
                        <div class="col">                             
                            <font style="display:block">Текст задачи</font>
                            <textarea name="content" style="min-width:400px;min-height:100px;"></textarea>                        
                        </div> 
                </div>
                <div class="row"> 
                        <div class="col"> 
                            <input type="submit" value="Добавить">                  
                        </div> 
                </div>
            </div>
            </form>
        </div>
    </body>
</html>
