<?php

class controller_task Extends Core_Controller {
    function __construct() {
        
    }

    public function head_page_view() {
         // пагинатор
         $CoreModel = new Core_Model();
         $counttasks = $CoreModel->count('tasks', true); 
         $this->data->countpages = intdiv ($counttasks, LIMIT);
         if (intdiv ($counttasks, LIMIT)<$counttasks/LIMIT) {
             $this->data->countpages++;
         } 
         // Сам контент 
         $sort = $_REQUEST['sort'];
         $limitstart = ($_REQUEST['layot']-1)*LIMIT;
         $limit = LIMIT;        
         $this->data->tasks = $CoreModel->read('tasks', true, $limitstart , $limit, $sort); 
         $this->view('index', "default");
    }
   
}

?>