<?php

class controller_task_admin Extends Core_Controller {
    function __construct() {
        
    }

    public function head_page_view() {               
        $this->data->errortext = '';        
        if (isset($_REQUEST['autorization'])) {           
            $this->auth();
        }       
        if (isset($_REQUEST['marktask'])) {           
            $this->marktask();
        }
        if (isset($_REQUEST['add'])) {                      
            $this->addContent();
        }     
        if (isset($_SESSION['user']) && $_SESSION['user']=='admin') {
            if (isset($_REQUEST['action']) && $_REQUEST['action']=='add') {
                $this->view('addcontent', "default");
            } else {
                $CoreModel = new Core_Model();
                // пагинатор
                $counttasks = $CoreModel->count('tasks', true);        
                $this->data->countpages = intdiv ($counttasks, LIMIT);
                if (intdiv ($counttasks, LIMIT)<$counttasks/LIMIT) {
                    $this->data->countpages++;
                } 
                // Сам контент 
                $sort = $_REQUEST['sort'];
                $limitstart = ($_REQUEST['layot']-1)*LIMIT;
                $limit = LIMIT;        
                $this->data->tasks = $CoreModel->read('tasks', true, $limitstart , $limit, $sort); 
                $this->view('index_admin', "default");
            }
        } else {
            $this->view('index_auth', "default");
        }
    }
    

    private function auth() {
        $this->data->errortext = '';
        if ($_POST['login']=='admin' && $_POST['password']=='123') {           
            $_SESSION['user']='admin';
        } else {
            $this->data->errortext = 'Не правильные реквизиты входа.';
        }
    }

    private function marktask() {       
        $CoreModel = new Core_Model();
        $task = $CoreModel->read('tasks', 'id='.$_REQUEST['marktask'].'', 0 , 1, 'id')[0];
        $update = []; 
        if ($task->status==1) {
            $update['status'] = 0;
        } else {
            $update['status'] = 1;
        }    
        $where = 'id='.$_REQUEST['marktask'];
        $CoreModel->update('tasks', $where, $update);
    }

    private function addContent() {       
        $CoreModel = new Core_Model();
        $update = [];
        $update['user'] = htmlspecialchars($_REQUEST['user']);
        $update['email'] = htmlspecialchars($_REQUEST['email']);
        $update['content'] = htmlspecialchars($_REQUEST['content']); 
        $update['status'] = 0;   
        $result = $CoreModel->add('tasks', $update);
        if ($result!=false) {
            unset($_REQUEST['action']);
        }
    } 
}

?>