<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="content-language" content="ru">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>
    <body>
        <div style="width:100%;padding-top:20px;padding-bottom:50px;">
            <div style="float:left;padding-left:30px;"><h1>Test.loc</h1></div>
            <div style="float:right;padding-right:30px;"><a href="<?php echo SITE_NAME;?>/log-in?action=add" style="margin-right:20px;">Добавить</a><a href="<?php echo SITE_NAME;?>" style="margin-right:20px;">Выйти</a></div>
            <div style="clear:both;"></div>
        </div>
       
        <div style="width:100%;padding-top:20px;padding-bottom:50px;">
            <div class="container" style="margin-bottom:20px;">
                <div class="row">
                    <div class="col">
                    <?php for($i=1;$i<=$this->data->countpages;$i++) :?>
                        <a href="<?php echo SITE_NAME;?>/log-in?sort=<?php echo $_REQUEST['sort'];?>&layot=<?php echo $i; ?>" style="margin-right:10px;<?php if($_REQUEST['layot']==$i) { echo 'color:#ff4444;';} else { echo 'color:#4444ff;'; } ?>"><?php echo $i; ?></a>
                    <?php endfor; ?>	
                        <font style="margin-left:30px;margin-right:10px;">сортировать по: </font>
                        <a href="<?php echo SITE_NAME;?>/log-in?sort=user&layot=1" style="margin-right:10px;<?php if($_REQUEST['sort']=='user') { echo 'color:#ff4444;';} else { echo 'color:#4444ff;'; } ?>">имя пользователя</a>
                        <a href="<?php echo SITE_NAME;?>/log-in?sort=email&layot=1" style="margin-right:10px;<?php if($_REQUEST['sort']=='email') { echo 'color:#ff4444;';} else { echo 'color:#4444ff;'; } ?>">email</a>
                        <a href="<?php echo SITE_NAME;?>/log-in?sort=status&layot=1" style="margin-right:10px;<?php if($_REQUEST['sort']=='status') { echo 'color:#ff4444;';} else { echo 'color:#4444ff;'; } ?>">статус</a>
                    </div>
                </div>
            </div>
            <div class="container">
                <?php foreach($this->data->tasks as $task) :?>
                <div class="row">
                    <div class="col">
                        <div style="<?php if($task->status==1) { echo 'background:#e1ffe1;';} else {echo 'background:#efefef;';} ?>;border-bottom:1px solid #e1e1e1;padding:10px 10px 0px 10px;">
                        <h3><?php echo $task->user; ?></h3>
                        <h5><?php echo $task->email; ?></h5>
                        <font style="display:block;padding-top:10px;"><?php echo $task->content; ?></font>
                        <div style="margin-bottom:10px;">
                            <form action="" method="post">
                                <input type="hidden" name='marktask' value="<?php echo $task->id; ?>">
                                <?php if ($task->status==0) : ?>
                                    <input type="submit" value="Выделить"> 
                                <?php else : ?>
                                    <input type="submit" value="Отменить выделение"> 
		                        <?php endif; ?>	
                            </form>
                        </div>
                        </div>                        
                    </div>
                </div>
                <?php endforeach; ?>	
            </div>
        </div>
    </body>
</html>
