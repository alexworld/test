<?php

    Class Core_Model { 
        public function __construct() {
     
            // объект бд коннекта 
            global $dbObject;
            $this->db = $dbObject;           
           
        }

        function read($table,$where,$limitstart, $limit,$order) {
            $query = "Select * From ".$table." Where ".$where." Order by ".$order." LIMIT ".$limitstart." ,".$limit."";
            $result = $this->db->query($query)->fetchAll(PDO::FETCH_OBJ);
            return $result;
        }

        function count($table, $where) { 
            $query = "Select COUNT(`id`) From ".$table." Where ".$where."";       
            return $this->db->query($query)->fetch(PDO::FETCH_ASSOC)['COUNT(`id`)'];
        }

        function update($table,$where, $data) {
            $data = $this->pdoSet($data);
            $query = "Update ".$table." Set ".$data['sql']." Where ".$where."";
            $result = $this->db->prepare($query)->execute($data['array']);           
            return $result;
        }

        function add($table,$data) {
            $data = $this->pdoSet($data);
            $query = "Insert Into ".$table." Set ".$data['sql']."";            
            //$result = $this->db->query($query);
            $result = $this->db->prepare($query)->execute($data['array']);               
            return $result;
        }

        private function pdoSet($data) {
            $sql = "";
            $arr = [];
            $keys = array_keys($data);
            foreach ($keys as $key) {                
                    $sql .= $key." = :".$key.", ";
                    $arr[":".$key] = $data[$key]; 
            }
            $sql = substr($sql, 0, -2); 
            return ["sql" => $sql, "array" => $arr];
        } 


    }


?>