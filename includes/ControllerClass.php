<?php

class Core_Controller {    
    function __construct($page) {
        global $dbObject;
        session_start();
        $dbObject = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS); 
        $dbObject->exec('SET CHARACTER SET utf8');       
        $this->db = $dbObject;
        $sort_field = array('user','email','status');
        if (!isset($_REQUEST['sort'])) {
            $_REQUEST['sort'] = 'user';
        } else {
            if (!in_array($_REQUEST['sort'], $sort_field)) {
                $_REQUEST['sort'] = 'user';
            }
        }
        if (!isset($_REQUEST['layot'])) {
            $_REQUEST['layot'] = 1;
        } else {
            $_REQUEST['layot'] = (int)$_REQUEST['layot'];
        }
        $this->page = $page;
    }

    public function content() { 
        $this->Loader();
        if ($this->page=='task') {
            $this->view_task_content();
        } else {
            if ($this->page=='task_admin') {
                $this->view_task_admin_content();
            }
        }
            
    }

    public function view_task_content() {
        $task = new controller_task();
        $task->head_page_view();   
    }

    public function view_task_admin_content() {
        $task = new controller_task_admin();
        $task->head_page_view();
    }

    public function Loader(){
        spl_autoload_register(['ClassLoader', 'autoload'], true, true);        
    }


    public function content_admin() {       
        $this->data->errortext = '';        
        if (isset($_REQUEST['autorization'])) {           
            $this->auth();
        }       
        if (isset($_REQUEST['marktask'])) {           
            $this->marktask();
        }
        if (isset($_REQUEST['add'])) {                      
            $this->addContent();
        }     
        if (isset($_SESSION['user']) && $_SESSION['user']=='admin') {
            if (isset($_REQUEST['action']) && $_REQUEST['action']=='add') {
                $this->view_admin('addcontent', "default");
            } else {
                $CoreModel = new Core_Model();
                // пагинатор
                $counttasks = $CoreModel->count('tasks', true);        
                $this->data->countpages = intdiv ($counttasks, LIMIT);
                if (intdiv ($counttasks, LIMIT)<$counttasks/LIMIT) {
                    $this->data->countpages++;
                } 
                // Сам контент 
                $sort = $_REQUEST['sort'];
                $limitstart = ($_REQUEST['layot']-1)*LIMIT;
                $limit = LIMIT;        
                $this->data->tasks = $CoreModel->read('tasks', true, $limitstart , $limit, $sort); 
                $this->view_admin('index_admin', "default");
            }
        } else {
            $this->view_admin('index_auth', "default");
        }
    }

    public function view($in_view, $in_theme = 'default') { 
        $contentPage = TEMPLATES_DIR.'/'.$in_theme.'/'.$in_view.'.php';        
        if (file_exists($contentPage) == false) {
            $contentPage = TEMPLATES_DIR.'/default/'.$in_view.'.php';
        }
        require ($contentPage); 
    }



}

?>